#include <string>
#include <algorithm>
#include <iostream>
#include <map>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
    // problem 8a
    std::string text = argv[1], newtext=text;
    int d = 'A'-'a';
    std::transform(text.begin(), text.end(), newtext.begin(),
		[&](char ch)->char { 
			if (ch>='a' && ch<='z')return ch+d;
                        else return ch;
		});
    std::cout << newtext << std::endl;

    // problem 8b
    map<string,vector<string>> courses;
    courses["CIS330"] = {"5324","1124","1251","1215"};
    courses["CIS315"] = {"1124","1251","2115"};
    return 0;
}

