#include <map>
#include <string>
#include <iostream>

using namespace std;
// part c
struct A {
    int num;
public:
    A(int i) : num(i) {};
    int getNumber() { return num; }
};
class B : public A {
    int decrement() { return --num; }
};

int main() {
   // part b
   map<int, string> m;
   m[4] = "John Doe";
   for (auto i : m) 
      cout << i.second << endl;
}
   
