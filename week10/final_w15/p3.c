#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main() {
  srand(time(NULL));
  int n = rand() % 50;
  int guess = 0, counter = 0;
  printf("Please enter a number (between 0 and 50): ");
  while (counter < 6 && guess != n) {
    scanf("%d",&guess);
    if (guess == n) {
      printf("You guessed correctly!\n");
      break;
    }
    else if (guess < n) printf("Too low! ");
    else printf("Too high! ");
    printf("Guess again -- enter a number: ");
    counter++;
  }
  return 0;
}
