#include <iostream> 
#include <fstream>
#include <istream>
#include <iterator>
#include <algorithm>

using namespace std;
class FileStats {
public:
  void readFile(string name) {
    string input = "";
    string punct = ".!?";
    string s = "";
    int sum = 0, count = 0;
    ifstream file(name);
    while (file >> s) {
      input += " " + s;
      if (punct.find(s[s.length()-1]) != string::npos) {
        sum += input.length();
        count++;
        input = "";
      }
    }
    file.close();
    stats = pair<int,float>(count,(float)sum/count);
  }

  void writeStats(string name) {
    ofstream file(name, ofstream::out);
    if ( file.is_open() ) {
      file << stats.first << endl << stats.second << endl;
      file.close();
    }
  }
  pair<int,float> getStats() { return stats; }
private:
  pair<int,float> stats;
};

int main(int argc, char *argv[]) {
  FileStats fs;
  fs.readFile("test.txt");
  cout << "Stats: " << fs.getStats().first << ", "
      << fs.getStats().second << endl;
  fs.writeStats("test.txt.stats");
  return 0;
}    
