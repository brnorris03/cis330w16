#include <stdio.h>
#include <string.h>
void func(char *ptr){ 
    strcpy(ptr, "Bye!"); 
} 

int main() { 
    char s[10] = "Hi!"; 
    func(s); 
    printf("%s\n", s); 
    return 0;
}

