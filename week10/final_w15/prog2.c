#include <stdio.h>
#include <string.h>
void func(char *ptr){ 
    ptr = "Oops!"; 
} 

int main() { 
    char *s = "Hi!"; 
    func(s); 
    printf("%s\n", s); 
    return 0;
}

