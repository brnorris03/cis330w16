#include <cstdlib>
#include <ctime>
#include <iostream>

class Dice {
public:
     Dice(int sides);
     bool operator<(const Dice& other); 
     void print() {std::cout << die1 << ", " << die2 << std::endl;}
private: 
     int die1;
     int die2; 
}; 

Dice::Dice(int sides) {
   die1 = (rand() % sides) + 1;
   die2 = (rand() % sides) + 1;
}


bool Dice::operator<(const Dice& other) {
    return (this->die1 + this->die2) < (other.die1 + other.die2);
}


int main() {
   srand(time(NULL));
   Dice p1(6), p2(6);

   std::cout << "p1 = "; p1.print(); 
   std::cout << "p2 = "; p2.print(); 
   std::cout << "p1 < p2? " << (p1 < p2) << std::endl;
}
