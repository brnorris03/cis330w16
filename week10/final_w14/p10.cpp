#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
/*
 * Implement the matching pennies game with the following rules.
 * The game is played between two players, Player A (human) and Player B (computer).
 * Each player has a penny and must secretly turn the penny to heads or tails.
 * The players then reveal their choices simultaneously. If the pennies
 * match (both heads or both tails) Player A keeps both pennies, so wins one
 * from Player B (+1 for A, -1 for B). If the pennies do not match (one heads
 * and one tails) Player B keeps both pennies, so receives one from
 * Player A (-1 for A, +1 for B). Assume each player starts with 5 pennies.
 *
 * You can use either C or C++. In your implementation, the computer randomly
 * chooses heads or tails. The user inputs either "heads" or "tails" (after flipping
 * a coin). The game continues until the user inputs "quit" or when either player runs
 * out of pennies. Before exiting, the program prints the total number of pennies for
 * both players. Here is an example game session:

Computer has 5 pennies, and you have 5 pennies.
Heads or tails (quit to exit)? heads
Computer picked tails. Computer wins this round. [You: 4, Computer: 6]
Heads or tails (quit to exit)? heads
Computer picked heads. You win this round. [You: 5, Computer: 5]
Heads or tails (quit to exit)? tails
Computer picked tails. You win this round. [You: 6, Computer: 4]
Heads or tails (quit to exit)? quit
Game over. You have 6 pennies, and the computer has 4 pennies. You won!
 *
 */

using namespace std;

// helper function to convert string to lowercase (not expected on final)
string toLower(string s) {
	string newstring = s;
	const int diff='a'-'A';
	for(int i = 0; i < s.length(); i++)
	    if( (s[i] >= 'A') && (s[i] <= 'Z') )
	    	newstring[i] += diff;
	return newstring;
}

int main() {
	// Initialize random number generator
	srand(time(NULL));
	string playerAchoice = "", playerBchoice = "";
	string choices[] = {"heads","tails"};
	int penniesA = 5, penniesB = 5;

	cout << "Cpmputer has " << penniesB << "pennies, and you have "
		 << penniesA << " pennies." << std::endl;


	while (playerAchoice != "quit" and penniesA and penniesB) {
		// No input checking required here, but would be nice to see
		while (playerAchoice != "heads" and playerAchoice != "tails" and playerAchoice != "quit") {
			cout << "Heads or tails (type quit to exit)? ";
			cin >> playerAchoice;
			playerAchoice = toLower(playerAchoice);
		}
		if (playerAchoice == "quit") break;
		// Randomly pick computer's choice
		playerBchoice = choices[rand() % 2];
		cout << "Computer picked " << playerBchoice;

		// Player A (user), player B (computer) match
		if (playerAchoice == playerBchoice) {
			penniesA += 1; penniesB -=1;
			cout << ". You win this round. ";
		} else {
			penniesA -= 1; penniesB +=1;
			cout << ". Computer wins this round. ";

		}
		cout << "[You: " << penniesA << ", Computer: " << penniesB << "]" << std::endl;
		playerAchoice = ""; // reset
	}


}


