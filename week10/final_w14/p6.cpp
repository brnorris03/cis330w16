#include <iostream> 
class A { 
public: 
    A(int i) : counter(i) {} 
    virtual void dec() { counter--; } 
    int getCounter() { return counter; } 
protected: 
    int counter; 
}; 
 
class B: public A { 
public: 
    B(int i) : A(i) {} 
    virtual void dec() { counter -= 3; }
}; 
 
int main() { 
    using namespace std; 
    A *x = new B(7); 
    x->dec(); 
    cout << x->getCounter() << std::endl;
    B y(4);
    y.dec();
    cout << y.getCounter() << std::endl;
} 
