#include <string>
#include <algorithm>
#include <iostream>

int main() {
    std::string letters = "abcdefghijklmnopqrstuvwxyz";
    std::string text = "zoo";
    const int len = letters.length();
    std::transform(text.begin(), text.end(), text.begin(),
		[&](char ch)->char { 
		return letters[(3 + letters.find(ch)) % len]; 
		});
    std::cout << text << std::endl;
}

