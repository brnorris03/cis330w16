#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
  char *userInput = NULL; 
  int count = 0;
  
  userInput = (char *) malloc (10 * sizeof(char)); 
  printf("Do you like snow? (yes/no) ");
  scanf("%s",userInput);

  while (  strcmp(userInput,"yes")  ) {
    printf("That sucks, try again.\n");
    printf("Do you like snow? (yes/no) ");
    scanf("%s",userInput);
    ++count;
  } 

  ++count;
  printf("Let's go skiing!\n");
  printf("The count is %d.\n", count);

  free(userInput);
  return 0;
}
