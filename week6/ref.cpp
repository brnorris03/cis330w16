#include <vector>
#include <iostream>

using namespace std;

std::vector<int> f1 () {

    vector<int> v(4,3);
    return v;
}

void f2( std::vector<int>& v ) {
   cout.width(10);
   for (auto it = v.begin(); it != v.end(); it++) 
      cout << *it << " ";
   cout << endl;
}

int main() {


    vector<int> v1 = f1();

    f2(v1);

    return 0;
}
