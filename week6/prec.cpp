#include <iostream>
int main() {
  double a = 3.1415926534;
  double b = 2006.0;
  double c = 1.0e-10;

  int oldprec = std::cout.precision();
  std::cout << "Old precision: " << oldprec << std::endl;
  std::cout.precision(2);

  std::cout << "default:\n";
  std::cout << a << '\n' << b << '\n' << c << '\n';

  std::cout << '\n';

  std::cout << "fixed:\n" << std::fixed;
  std::cout << a << '\n' << b << '\n' << c << '\n';

  std::cout << '\n';

  std::cout << "scientific:\n" << std::scientific;
  std::cout.precision(oldprec);
  std::cout << a << '\n' << b << '\n' << c << '\n';
  return 0;
}
