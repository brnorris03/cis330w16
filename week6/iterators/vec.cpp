#include <iostream>
#include <vector>
#include <set>

void incfirst(std::vector<int>& v) {
  (v[0])++;
}

int main ()
{
  std::vector<int> myvector (5);  // 5 default-constructed ints
  auto v2 = myvector;

  // Iterate forwards, initializing the vector
  std::cout << "forward:";
  int i=0;

  //std::vector<int>::iterator it = myvector.begin();
  for (auto it = myvector.begin(); it!= myvector.end(); ++it) {
    // it is a pointer to the current object
    *it = (double)++i + 0.23;   
    std::cout << ' ' << (*it);
  }
  std::cout << std::endl;

  incfirst(myvector);

  // Iterate backwards
  std::cout << "backward:";

  int sum = 0;
  //std::vector<int>::reverse_iterator rit = myvector.rbegin();
  for (auto rit = myvector.rbegin(); rit!= myvector.rend(); ++rit) {
    // rit is a pointer to the current object
    std::cout << ' ' << (*rit); 
  }

  for (int i = 0; i < static_cast<int>(myvector.size()); i++) 
    sum += myvector[i];
  std::cout << std::endl << "Sum = " << sum << std::endl;


  int vecsize = myvector.size();
  
  std::cout << std::endl << "Size = " << vecsize << std::endl;

  std::set<int> myset;

  for (int i = 0; i < 10; i++) {
     myset.insert(i); 
  }
  for (int i = 0; i < 10; i++) {
     myset.insert(i); 
  }
  for (auto it = myset.begin(); it != myset.end(); it++) 
     std::cout << *it << std::endl;

  return 0;
}
