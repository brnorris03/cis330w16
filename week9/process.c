#include <sys/types.h>
#include <unistd.h>
pid_t  fork(void);

int main() {
    int v = 42;
    pid_t pid;
    if ((pid = fork()) < 0) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        // Child code
        printf("Child: child %d of parent %d\n",
                getpid(), getppid());
        v++;
        printf("Child: v = %d\n", v);
        sleep(2);
    } else {
        // Parent code
        printf("Parent: Bye, child (%d)! v = %d\n",pid, v);
        int status;
        // Wait for any child
        if (waitpid(-1,&status,0) == pid) {
            printf("Parent: Welcome back, child %d!\n", pid);
        }
        sleep(20);
    }
    return 0;
}
