/* signal example */
#include <cstdio>      /* printf */
#include <csignal>     /* signal, raise, sig_atomic_t */
#include <unistd.h>

sig_atomic_t signaled = 0;

void my_handler (int param)
{
  signaled = 1;
}

int main ()
{
  void (*prev_handler)(int);

  prev_handler = signal (SIGINT, my_handler);

  while (! signaled) {
     // First, wait for signal, and if not raised (e.g., with Ctrl-C), raise it internally
     sleep(10);
     raise(SIGINT);
  }
  
  printf ("signaled is %d.\n",signaled); // 1 if signal was caught, 0 otherwise
  

  return 0;
}
