#include <stdio.h>
#include "point.h"

void movePoint_wrong(struct Point p, const int dist) {
  p.x += dist;
  p.y += dist;
}

void movePoint(struct Point *p, const int dist) {
  p->x += dist;
  p->y += dist;
}

void printPoint(struct Point p) {
  printf("x=%d, y=%d\n", p.x, p.y);
}


