#include <iostream>
class Position
{
public:
    Position(int row=0) : x(row), y(0)
    {
        secretStuff = false;
        std::cout << "Called constructor with no arguments.\n";
    }

    Position(int row, int col): Position() 
    {
        x = row;
        y = col;
        std::cout << "Called constructor with two arguments: " << row << ", " << col << std::endl;
    }

    ~Position() {
	std::cout << "Calling destructor." << std::endl;
    }
private:
    int x;
    int y;
    bool secretStuff;
};

int main() {

   Position p;

   Position p2(3,5);

}
