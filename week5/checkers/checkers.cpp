#include <iostream>

class Checkers {
public:
    Checkers(int size_p) : size(size_p) { 
	/* allocate the 2D board array here */ 
	board = new char*[size];
        for (int i = 0; i < size; i++) {
		board[i] = new char[size];
		for (int j = 0; j < size; j++) {
			board[i][j] = '_';
		}
	}
		
    };

    Checkers(const Checkers& rhs) {
	// Deep copy
	this->size = rhs.size;

	board = new char*[size];
        for (int i = 0; i < size; i++) 
		board[i] = new char[size];
        for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			this->board[i][j] = rhs.board[i][j];
		}
	}
    }

    void print() {
        for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			std::cout << board[i][j] << " ";
		}
		std::cout << std::endl;
	}
    }

    ~Checkers() { 
	/* free the 2D board array here */ 
        for (int i = 0; i < size; i++) 
		delete [] board[i];
	delete board;
    }


public:
    int size;
    char **board;
};

int main() {
    Checkers c1(8);
    c1.print();


    std::cout << std::endl;
    Checkers c2(c1);
    c2.board[2][5] = 'O';
    c2.print();


    Checkers c3 = c1;

    std::cout << "C3: " << std::endl;
    c3.print();

}
