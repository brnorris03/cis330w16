# README #

This repository contains codes from weekly lecture exercises and can be used by everyone in the class. Please follow the following conventions when adding code:

* Place it in the proper week directory, in a subdirectory named uniquely, e.g., week5/yourcisusername
* Do not add binaries or other generated files to git