// An example of subtype polymorphism
// http://www.catonmat.net/blog/cpp-polymorphism/
#include <iostream>
#include "cats.hpp"

using namespace std;

void do_meowing(Felid *cat) {
 cat->meow();
}

int main() {
 Cat Fluffy;
 Tiger Tigger;
 Ocelot Sam;

 //Fluffy.meow();
 //Tigger.meow();
 //Sam.meow();

 do_meowing(&Fluffy);
 do_meowing(&Tigger);
 do_meowing(&Sam);

 Felid *feline;
 
 feline = &Fluffy;
 Tiger *t = dynamic_cast<Tiger*>(feline);
 if (t == nullptr) {
   cout << "I don't eat human." << endl;
 } else {
   t->eat_human();
 }

 feline = &Tigger;
 t = dynamic_cast<Tiger*>(feline);
 if (t == nullptr) {
   cout << "I don't eat human." << endl;
 } else {
   t->eat_human();
 }

 feline = &Sam;
 t = dynamic_cast<Tiger*>(feline);
 if (t == nullptr) {
   cout << "I don't eat human." << endl;
 } else {
   t->eat_human();
 }

}
