#include <iostream>
#include <utility>
#include <vector>

using namespace std;

class Vessel {
  public:
    virtual void cruise() = 0; 
    void print() { cout << this->kind << endl; }
    string kind;
};

class Sailboat : public Vessel {
  public: 
    Sailboat()  { this->kind  = "Sailboat"; }
    virtual void cruise() {} 
    pair<int,int> pos;
};

class Destroyer : public Vessel {
  public:
    Destroyer()  { this->kind = "Destroyer"; amo = 100000.0; }
    void destroy() { cout << "Consider yourself destroyed" << endl; }
    virtual void cruise() {} 
    float amo;
};

void init(vector<Vessel*> &fleet) {
   fleet.push_back(new Sailboat());
   fleet.push_back(new Destroyer());
   fleet.push_back(new Sailboat());
   fleet.push_back(new Destroyer());
}

int main () {

   vector<Vessel*> fleet;

   // Initialize
   init(fleet);
 
   for (auto it = fleet.begin(); it != fleet.end(); it++) {
      (*it)->print();
      Destroyer *d = dynamic_cast<Destroyer*>( *it ); 
      if ( d != nullptr )
         d->destroy();
   }

   return 0;
}

