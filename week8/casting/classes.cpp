// implicit conversion of classes:
#include <iostream>
#include <typeinfo> // for typeid
using namespace std;

class A {};

class  B {
public:
  // conversion from A (constructor):
  B (const A& x) { 
     cout << "Constructor: Constructing a B from an A\n";
  }

  // conversion from A (assignment):
  B& operator= (const A& x) { 
     cout << "Assignment: Assigning an A to a B\n"; 
     return *this; }

  // conversion to A (type-cast operator)
  operator A() { 
     cout << "Type-cast: Converting to an A (type-cast operator)\n";
     return A(); 
  }
};

int main ()
{
  A foo;
  cout << "Type of foo: " << typeid(foo).name() << endl;

  B bar = foo;   // calls constructor
  cout << "Type of bar: " << typeid(bar).name() << endl;

  bar = foo;      // calls assignment
  cout << "Type of bar after assignment bar = foo: " << typeid(bar).name() << endl;

  foo = bar;      // calls type-cast operator
  cout << "Type of foo after assignment foo = bar: " << typeid(foo).name() << endl;

  return 0;
}
