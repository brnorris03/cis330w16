#include <vector>
#include <memory>    // unique_ptr
#include <algorithm> // sort
#include <iostream>


using namespace std;

struct indirect_less
{
	template <class T>
	bool operator()(const T& x, const T& y)
		{return *x < *y;}
};

int main() {
	vector<unique_ptr<int> > vi;
	vi.push_back(unique_ptr<int>(new int(0)));  // populate vector 
	vi.push_back(unique_ptr<int>(new int(3)));
	vi.push_back(unique_ptr<int>(new int(2)));
	sort(vi.begin(), vi.end(), indirect_less());  //result: {0, 2, 3}
	for (auto it = vi.begin(); it != vi.end(); ++it)
		cout << **it << endl;
}
