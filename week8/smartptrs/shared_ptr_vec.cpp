#include <iostream>
#include <memory>
#include <vector>

using namespace std;

class MyClass {
public:
    MyClass(){
        cout << "Creating MyClass object" << endl;
    }
    virtual ~MyClass(){
        cout << "Destroying MyClass object" << endl;
    }
    void method(){
        cout << "Called method of MyClass object" << endl;
    }
};

 
int main() {
    vector<shared_ptr <MyClass> > v;
    //Create a bunch of shared pointers
    shared_ptr<MyClass>
        m1(new MyClass), //Object #1
        m2(new MyClass), //Object #2
        m3(new MyClass); //Object #3
 
    //The vector will add one reference to each added object
    v.push_back(m1);
    v.push_back(m2);
    v.push_back(m3);
    //Every object now has 2 references
 
    m2.reset(); //Object #2 has 1 reference left
    v.erase(v.begin() + 1); //Object #2 has no reference left
 
    for (unsigned int i = 0; i <v.size(); ++i) {
        v[i]->method();
    }
    return 0;
}
