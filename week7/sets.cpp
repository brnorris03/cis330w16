#include <set>

#include <iostream>
#include <algorithm>    // for_each

#define present(container, element) (container.find(element) != container.end()) 

#define cpresent(container, element) (find(container.begin(), container.end(), element) != container.end())

using namespace std;

int main() {
	 set<int> s;

	 for(int i = 1; i <= 100; i++) {
	      s.insert(i); // Insert 100 elements, [1..100]
	 }

	 s.insert(42); // does nothing, 42 already exists in set

	 int sum = 0;
	 for (auto it = s.find(31); it != s.end(); it++) {
		 sum += *it;
	 }

	 cout << "I have the number 114: " << cpresent(s,114) << endl;
	 cout << "I have the number 14: " << (s.find(14) != s.end()) << endl;

	 //for(int i = 2; i <= 100; i += 2) {
	  //    s.erase(i); // Erase even values
	 //}

	 cout << "Set size: " << s.size() << endl; // n will be 50

	 // We can avoid hard-coding the size
	 for_each(s.begin(), s.end(),
			 [&s](int i)->void { if (i % 2 != 0) s.erase(i); });

	 cout << "Set size: " << s.size() << endl;

	 string b = "";
	 for_each(s.begin(), s.end(), 
			[] (int element)->void { 
			 	cout << element << " ";
			}
		);

	int sum2 = 0;
 	for (auto it = s.begin(); it != s.end(); it++) {
		sum2 += *it;
	}

   	//cout << b << endl;
	 
}
