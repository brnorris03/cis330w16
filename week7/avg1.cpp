#include <iostream>
#include <fstream>
#include <set>
#include <numeric>
#include <algorithm>

using namespace std;

int myplus(const int &a, const int &b) {
    return a + b;
}

int main() {
	ifstream a("data.txt");
	multiset<int> b;
	/* Read the data from the file. */
	int c;
	while (a >> c) b.insert(c);

        std::string s = std::accumulate(b.begin(), b.end(), std::string{},
                                    [&b](const std::string& a, int e) {
                                        return a.empty() ? std::to_string(e)
                                               : a + '-' + std::to_string(e);
                                    });

	cout << s << endl;
	/* Compute the average. */
	cout << accumulate(b.begin(), b.end(), 0.0, 
			[] (const int &a, const int &b) { return a + b; }) / b.size() << endl;

	cout << count_if(b.begin(), b.end(), [] (int a) { return a < 90; }) << endl;

	return 0;
}
