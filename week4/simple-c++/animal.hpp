#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_

class Animal {
public: 
	Animal();
	virtual ~Animal() = 0;
    virtual void eat() = 0;
    virtual void sleep() = 0;
    virtual void drink() = 0;
    int legCount();
    bool isCarnivorous();
    void die();
protected:
    int legs;
    bool carnivorous;
	bool alive;
};

#endif /* ANIMAL_HPP_ */
