class Animal {
public: 
    Animal();
    ~Animal();
    void eat();
    void sleep();
    void drink();
    bool isCarnivorous() { return carnivorous; }
protected:
    int legs;
    int arms;
    int age;
    bool carnivorous;
};

class Quadruped : public Animal {
public: 
    Quadruped() : Animal() { legs = 4; }
    void walk();
    void run();
};

class Sheep : public Quadruped {
public: 
    Sheep() : Quadruped() { carnivorous = false; }
    void growWool();
};

class Wolf : public Quadruped {
public:
    Wolf() : Quadruped() { carnivorous = true; }
    void hunt();
};

class Color {
public: 
    Color(); 
    ~Color();
    string getName(); 
    void setName(string);
private:
    string name;
};

class Wool {
public:
    Wool(Color);
    ~Wool();
    Color getColor() { return color; }
    Color getColor(Color c) { color = c; }
private:
    Color color;
};

class Sheep : public Quadruped {
public: 
    Sheep() : Quadruped() { carnivorous = false; }
    void growWool(Color c) { wool.setColor(c)};
private:
     Wool wool;
};


int main() {
   Sheep sheep1;
   Wolf wolf;
   sheep1.eat();
}
